// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: "http://18.223.146.209/api/v1/",
  firebaseConfig: {
    apiKey: "AIzaSyBgXrpdNMrn0Y99o5KJJyx3GNwMbui6ork",
    authDomain: "unexclass.firebaseapp.com",
    databaseURL: "https://unexclass.firebaseio.com",
    projectId: "unexclass",
    storageBucket: "unexclass.appspot.com",
    messagingSenderId: "998585298845",
    appId: "1:998585298845:web:e52cf2ea7e1faa73908974",
    measurementId: "G-TV5PZ3R2LT"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
