import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SkeletonComponent } from './components/skeleton/skeleton';
import { SkeletonModule } from 'primeng/skeleton';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { CardModule } from 'primeng/card';
import { CalendarModule } from 'primeng/calendar';
import { ButtonModule } from 'primeng/button';
import { MessagesModule } from 'primeng/messages';
import { InputTextModule } from 'primeng/inputtext'
import { ToastModule } from 'primeng/toast';
import { DynamicDialogModule } from 'primeng/dynamicdialog';
import { TableModule } from 'primeng/table';
import { KnobModule } from 'primeng/knob';
@NgModule({
  declarations: [SkeletonComponent],
  exports: [CommonModule, KnobModule, SkeletonComponent, CardModule, ProgressSpinnerModule, CalendarModule, ButtonModule, MessagesModule, InputTextModule, ToastModule, DynamicDialogModule, TableModule],
  imports: [
    CommonModule,
    SkeletonModule,
    ProgressSpinnerModule,
    CardModule,
    CalendarModule,
    ButtonModule,
    MessagesModule,
    InputTextModule,
    ToastModule,
    DynamicDialogModule,
    TableModule,
    KnobModule
  ]
})
export class SharedModule { }
