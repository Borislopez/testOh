import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Component({
    selector: 'app-skeleton',
    templateUrl: './skeleton.html',
})
export class SkeletonComponent implements OnInit {
    public isLoading$: BehaviorSubject<boolean>;

    constructor() {
        this.isLoading$ = new BehaviorSubject<boolean>(true);
    }
    ngOnInit(): void {
        //Hice esto en modo simulación para mostrar un loading mediante una carga de datos haciendo uso de un REQUEST u otra via
        setTimeout(() => {
            this.isLoading$.next(false);
        }, 3000)
    }

}
