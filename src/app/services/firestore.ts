
import {
    CollectionReference,
    DocumentData,
    addDoc,
    collection,
    deleteDoc,
    doc,
    updateDoc,
} from '@firebase/firestore';
import { Firestore, collectionData, docData } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '@interfaces/user';

@Injectable({
    providedIn: 'root',
})
export class FirestoreService {
    private usersCollection: CollectionReference<DocumentData>;

    constructor(private readonly firestore: Firestore) {
        this.usersCollection = collection(this.firestore, 'users');
    }

    getAll() {
        return collectionData(this.usersCollection, {
            idField: 'id',
        }) as Observable<User[]>;
    }

    get(id: string) {
        const userDocumentReference = doc(this.firestore, `users/${id}`);
        return docData(userDocumentReference, { idField: 'id' });
    }

    create(user: User) {
        return addDoc(this.usersCollection, user);
    }

    update(user: User) {
        const userDocumentReference = doc(
            this.firestore,
            `users/${user.id}`
        );
        return updateDoc(userDocumentReference, { ...user });
    }

    delete(id: string) {
        const userDocumentReference = doc(this.firestore, `users/${id}`);
        return deleteDoc(userDocumentReference);
    }
}
