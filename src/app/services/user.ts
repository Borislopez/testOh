import { Injectable } from '@angular/core';
import { UserInterface } from '@interfaces/user';
import { tap } from 'rxjs/operators';
import { UserState } from '@states/user';
import { Observable } from 'rxjs';
import { FirestoreService } from './firestore';

@Injectable()
export class UserService {

    constructor(
        private userState: UserState,
        private fsService: FirestoreService
    ) { }

    setAmountOfUser(amount: number): void {
        this.userState.setAmountOfUser(amount);
    }

    setAverageAge(average: number): void {
        this.userState.setAverageAge(average);
    }

    getUser(): Observable<UserInterface> {
        return this.userState.getUser();
    }

    getUsers(): Observable<UserInterface[]> {
        // @ts-ignore
        return this.userState.getUsers();
    }

    calculate(): void {
        this.getUsers().subscribe((users: UserInterface[]) => {
            let ages: (number | undefined)[] = users.map(user => user.age);
            const amountUser = users.length;
            this.setAmountOfUser(amountUser);
            if (users.length > 0) this.setAverageAge(this.getAverage(ages));

            this.getAmountOfUser().subscribe((data: any) => console.log(data))
        })
    }

    getAverage(users: any): number {
        let reducer = (total: number, currentValue: number) => total + currentValue;
        let sum = users.reduce(reducer)
        return sum / users.length;
    }

    getAmountOfUser(): Observable<number> {
        return this.userState.getAmountOfUser();
    }

    getAverageAge(): Observable<number> {
        return this.userState.getAverageAge();
    }

    loadUsers(): Observable<UserInterface[]> {
        try {
            return this.fsService.getAll().pipe(tap((users: UserInterface[]) => this.userState.setUsers(users)))
        } catch (error) {
            console.error(error);
            throw error;
        }
    }
}
