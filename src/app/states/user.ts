import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { UserInterface, User } from '@interfaces/user';

@Injectable()
export class UserState {
    private user$ = new BehaviorSubject<UserInterface>(new User());
    private users$ = new BehaviorSubject<UserInterface[]>([] as UserInterface[]);
    private amountOfUser$ = new BehaviorSubject<number>(0);
    private averageAge$ = new BehaviorSubject<number>(0);


    setUser(user: UserInterface): void {
        this.user$.next(user);
    }

    setUsers(users: UserInterface[]): void {
        this.users$.next(users);
    }

    setAmountOfUser(amount: number): void {
        this.amountOfUser$.next(amount);
    }

    setAverageAge(amount: number): void {
        this.averageAge$.next(amount);
    }

    getAmountOfUser(): Observable<number> {
        return this.amountOfUser$.asObservable();
    }

    getAverageAge(): Observable<number> {
        return this.averageAge$.asObservable();
    }

    getUser(): Observable<UserInterface> {
        return this.user$.asObservable();
    }

    getUsers(): Observable<UserInterface[]> {
        return this.users$.asObservable();
    }

    get userData(): UserInterface {
        return this.user$.value;
    }

    get users(): UserInterface[] {
        return this.users$.value;
    }

    get amountOfUser(): number {
        return this.amountOfUser$.value;
    }

    get averageAge(): number {
        return this.averageAge$.value;
    }

}
