import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormComponent } from '@components/form/form';
import { ListComponent } from '@components/list/list';
import { UserService } from '@services/user';

@Component({
  standalone: true,
  selector: 'app-register',
  imports: [CommonModule, ListComponent, FormComponent],
  templateUrl: './register.html',
})
export class RegisterContainer implements OnInit {

  constructor(private userService: UserService) {
  }

  ngOnInit(): void {
    this.userService.loadUsers().subscribe();
  }

}
