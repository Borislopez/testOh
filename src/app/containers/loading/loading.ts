import { Component } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
@Component({
  standalone: true,
  selector: 'app-loading',
  imports: [SharedModule],
  templateUrl: './loading.html',
})
export class LoadingContainer {
  constructor() {
  }
}
