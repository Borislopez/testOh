import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CountUserComponent } from '@components/count-user/count-user';
import { UserService } from '@services/user';
import { MessageService } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.html',
  providers: [DialogService, MessageService]
})
export class ModalContainer implements OnInit {

  ref?: DynamicDialogRef;

  constructor(public dialogService: DialogService, private router: Router, private userService: UserService) {
    this.userService.loadUsers().subscribe()
    this.userService.calculate();
  }

  ngOnInit(): void {
    this.openModal();
  }

  openModal(): void {
    this.ref = this.dialogService.open(CountUserComponent, {
      header: 'Información',
      width: '40%',
      contentStyle: { overflow: 'auto' },
      baseZIndex: 10000,
      maximizable: true
    });
    this.ref.onClose.subscribe(() => this.router.navigateByUrl('/register'));
  }

}
