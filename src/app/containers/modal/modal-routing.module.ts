import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ModalContainer } from './modal';

const routes: Routes = [
    {
        path: '',
        component: ModalContainer
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class ModalRoutingModule {
}
