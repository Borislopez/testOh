import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DialogService } from 'primeng/dynamicdialog';
import { ModalContainer } from './modal';
import { ModalRoutingModule } from './modal-routing.module';

@NgModule({
    declarations: [
        ModalContainer
    ],
    imports: [
        CommonModule,
        ModalRoutingModule
    ],
    providers: [
        DialogService
    ],
})
export class ModalModule {
}
