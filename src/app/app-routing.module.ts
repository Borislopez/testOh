import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ModalModule } from '@containers/modal/modal.module';

const routes: Routes = [
  {
    path: '',
    title: 'Loading  - Pantalla 1',
    loadComponent: () => import('@containers/loading/loading')
      .then(m => m.LoadingContainer),
  },
  {
    path: 'register',
    title: 'Registrar usuarios - Pantalla 2',
    loadComponent: () => import('@containers/register/register')
      .then(m => m.RegisterContainer)
  },
  {
    path: 'modal',
    title: 'Modal - Pantalla 3',
    loadChildren: () => {
      return ModalModule;
    }
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
