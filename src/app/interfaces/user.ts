export interface UserInterface {
    id: string;
    name: string;
    lastName: string;
    email: string;
    password?: string;
    age?: number;
    dateOfBirth?: Date;
}

export class User implements UserInterface {
    constructor(public id = '', public name = '', public lastName = '', public email = '') {
    }
}