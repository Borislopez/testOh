import { Component, OnDestroy, OnInit } from '@angular/core';
import { UserInterface } from '@interfaces/user';
import { UserService } from '@services/user';
import { SharedModule } from '@shared/shared.module';
import { BehaviorSubject, Subscription } from 'rxjs';

@Component({
  selector: 'app-list',
  templateUrl: './list.html',
  standalone: true,
  imports: [SharedModule]
})
export class ListComponent implements OnInit, OnDestroy {
  public isLoading$: BehaviorSubject<boolean>;
  isLoading = true;
  users: UserInterface[] = [];
  subscriptions = new Subscription();
  columnsTable = ['Nombre',
    'Apellido',
    'Email',
    'Edad',
    'Fecha de nacimiento'];

  constructor(private userService: UserService) {
    this.isLoading$ = new BehaviorSubject<boolean>(true);
  }

  ngOnInit(): void {
    this.getUsers();
  }

  getUsers(): void {
    this.subscriptions.add(
      this.userService.getUsers().subscribe((users: UserInterface[]) => {
        this.users = users
        if (users.length > 0) this.isLoading$.next(false);
      }))
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

}
