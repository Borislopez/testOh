import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { UserService } from '@services/user';
import { SharedModule } from '@shared/shared.module';

@Component({
    selector: 'app-count-user',
    templateUrl: './count-user.html',
    standalone: true,
    imports: [SharedModule, FormsModule]
})
export class CountUserComponent implements OnInit {
    public amountOfUser = 0;
    public averageAge = 0;

    constructor(private userService: UserService) { }

    ngOnInit(): void {
        this.userService.getAmountOfUser().subscribe((amount: number) => this.amountOfUser = amount);
        this.userService.getAverageAge().subscribe((average: number) => this.averageAge = parseInt(average.toFixed(2)));
    }
}

