import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { FirestoreService } from '@services/firestore';
import { SharedModule } from '@shared/shared.module';
import { MessageService } from 'primeng/api';
import { Router } from '@angular/router';
@Component({
  selector: 'app-form',
  templateUrl: './form.html',
  standalone: true,
  imports: [ReactiveFormsModule, SharedModule],
  providers: [MessageService]
})
export class FormComponent implements OnInit {
  public cardHeader: string = 'Registrar usuarios'
  public registerForm: FormGroup = new FormGroup({});

  constructor(private fb: FormBuilder, private fsService: FirestoreService, private messageService: MessageService, private router: Router) {

  }

  ngOnInit(): void {
    this.buildForm();
  }

  buildForm(): void {
    this.registerForm = this.fb.group({
      name: ['', Validators.required],
      lastname: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      age: ['', Validators.required],
      password: ['', [Validators.required]],
      dateOfBirth: [Date, Validators.required]
    })
  }

  submit(): void {
    this.fsService.create(this.registerForm.value).then(() => {
      this.messageService.add({ severity: 'success', summary: 'Listo', detail: 'Usuario registrado con exito' });
      this.registerForm.reset();

    })
  }

  openModal(): void {
    this.router.navigateByUrl('/modal');
  }

}
