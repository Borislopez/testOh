/// <reference types="cypress" />
describe('Register page tests', () => {
    beforeEach(() => {
        cy.visit('localhost:4200/register')
    })

    it('should load Register page and create user with list', () => {
        cy.get('[data-cy=app-form').should('exist');
        cy.get('[data-cy=app-list').should('exist');
        cy.get('[data-cy=input-name').should('exist').type('Cypress name test');
        cy.get('[data-cy=input-lastname').should('exist').type('Cypress lastname test');
        cy.get('[data-cy=input-email').should('exist').type('cypress@gmail.com');
        cy.get('[data-cy=input-password').should('exist').type('Cypress name test');
        cy.get('[data-cy=input-age').should('exist').type('123');
    })
})
