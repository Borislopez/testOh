/// <reference types="cypress" />
describe('Modal page tests', () => {
    beforeEach(() => {
        cy.visit('localhost:4200/register')
    })

    it('should load Register page and create user with list', () => {
        cy.get('[data-cy=app-form').should('exist');
        cy.get('[data-cy=app-list').should('exist');
        cy.get('[data-cy=btn-open-modal').should('exist').click();
        cy.get('[data-cy=amountOfUser').should('exist');
        cy.get('[data-cy=averageAge').should('exist');
    })
})