/// <reference types="cypress" />
describe('Loading page tests', () => {
    beforeEach(() => {
        cy.visit('localhost:4200')
    })

    it('should load Modal page', () => {
        cy.get('[data-cy=title-content').should('exist');
        cy.get('[data-cy=app-skeleton').should('exist');
    })
})
